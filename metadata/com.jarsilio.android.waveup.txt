Categories:System
License:GPLv3
Web Site:https://gitlab.com/juanitobananas/wave-up
Source Code:https://gitlab.com/juanitobananas/wave-up/tree/HEAD
Issue Tracker:https://gitlab.com/juanitobananas/wave-up/issues

Auto Name:WaveUp
Summary:Turn the display on by waving
Description:
Wake up your phone - switch the screen on - when you wave over the proximity
sensor.

The author developed this app because he wanted to avoid pressing the power
button just to take a look at the watch - which happens rather often. The app is
inspired by the proprietary "Gravity Screen On/Off":

Just wave your hand over the proximity sensor of your phone to turn the screen
on. This is called wave mode and can be disabled in the settings screen in order
to avoid accidental switching on of your screen.

It will also turn on the screen when you take your smartphone out of your pocket
or purse. This is called pocket mode and can also be disabled in the settings
screen.

Both of these modes are enabled by default.

It also locks your phone and turns off the screen if you cover the proximity
sensor for one second. This does not have a special name but can nonetheless be
changed in the settings screen too. It is also enabled by default.

For those who have never heard proximity sensor before: it is a small thingie
that is somewhere near where you put your ear when you speak on the phone. You
practically can't see it and it is responsible for telling your phone to switch
off the screen when you're on a call.
.

Repo Type:git
Repo:https://gitlab.com/juanitobananas/wave-up.git

Build:0.91,1
    commit=875a239bf71de7cbba9369d57a53bbf1b286e567
    subdir=app
    gradle=yes

Build:0.92,2
    commit=v0.92
    subdir=app
    gradle=yes

Build:0.93,3
    commit=v0.93
    subdir=app
    gradle=yes

Build:0.94,4
    commit=v0.94
    subdir=app
    gradle=yes

Build:0.95,5
    commit=v0.95
    subdir=app
    gradle=yes

Build:0.96,6
    commit=v0.96
    subdir=app
    gradle=yes

Build:0.96-1,7
    commit=v0.96-1
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.96-1
Current Version Code:7
